## История изменений

### Release 8.0.0
- Версия sdk appUpdate 8.0.0.

### Release 7.0.0
- Версия sdk appUpdate 7.0.0.

### Release 6.1.0
- Версия sdk appUpdate 6.1.0.

### Release 6.0.0
- Версия sdk appUpdate 6.+.

### Release 3.0
- Версия sdk appUpdate 3.+.
- Обновлены методы установки обновления.

### Release 2.0
- Версия SDK appUpdate 2.+
- Добавлен проект .aar пакетов плагинов

### Release 1.0.1
- Версия SDK appUpdate 1.0.1

### Release 1.0
- Версия SDK appUpdate 1.0.0

### Release 0.2
- Версия SDK appUpdate 0.2.0
- Версия SDK core 0.1.10
